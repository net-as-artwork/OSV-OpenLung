![Logo](images/OL_BANNER.png)

This document in other languages:

|[català](README-ca.md)|[español](README-es.md)|[日本語](README-ja.md)|[汉语](README-yh-Hant.md)|
|---|---|---|---|

# Low Resource Bag Valve Mask (BVM) Ventilator

- This project was jumpstarted by the COVID-19 global pandemic as a result of community discussion on a facebook group called Open Source COVID19 and OpenSourceVentilator, this is why I created a GitLab project for a new open source product called **OpenLung**.
- More specifically in a discussion surrounding low cost **Bag Valve Mask** (**BVM** or **AmbuBag**) based emergency respirators wherein prior solutions had been developed. [The first from an MIT research group](https://web.mit.edu/2.75/projects/DMD_2010_Al_Husseini.pdf) comprising of the following persons (Abdul Mohsen Al Husseini, Heon Ju Lee, Justin Negrete, Stephen Powelson, Amelia Servi, Alexander Slocum and Jussi Saukkonen). [The second device from a Rice University Mechanical Engineering student group](http://oedk.rice.edu/Sys/PublicProfile/47585242/1063096) comprising of the following persons (Madison Nasteff, Carolina De Santiago, Aravind Sundaramraj, Natalie Dickman, Tim Nonet and Karen Vasquez Ruiz. Photo by Jeff Fitlow).
- This project seeks to combine and improve the efforts of these two projects into a more simple and reliable device that consists mostly of 3D printed parts.

*WARNING/DISCLAIMER: Whenever possible, please seek professional medical care with proper equipment setup by trained individuals. Do not use random information you found on the internet. We are not medical professionals, just people on the internet.*

# Reasons for using an Ambu-Bag

- Mass Produced
- Certified components
- Small and Simple mechanical requirements
- Previous research and testing in this area
- Adaptable to both invasive tubing and masks

# Project Requirements
- Project Requirements are listed [here](documentation/design-requirements.md).

# Project Progress

![Current Mechanical Concept](images/CONCEPT_6_MECH.png)
*Current Design Concept 6 with known issues*

# TO-DO (Where we need help)

*If you can assist in these areas, please clone or fork this repo and create a merge request with your new/modified files.*

- ~~Design a more integral 3D printed actuation mechanism~~
- ~~Spec a good motor~~
- Spec an interface (LCD and Buttons)
- Spec feedback sensors for PEEP, low voltage, high and low pressure events.
- Outline interface visually

## IN PROGRESS (What we are working on today, March 17, 2020)

- Organization of Repository structure
- ~~Updated Ambu-Bag CAD model~~
- Interactivity outline
- Building communication bridge to the [open source ventilator project Ireland](https://opensourceventilator.ie/)
- Assessing current communication and deligation methods

## COMPLETED (What has been completed)

- ~~Design requirements~~

## Next iterative improvements

- ~~Perpendicular Motor/Drive shaft connection due to axial load issues~~
- ~~Simplify transmission struction~~
- ~~Further compacting of overall build~~

